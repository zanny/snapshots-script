#! /usr/bin/env python3
"""
Automated btrfs-snapshot script. Expects snapshots to exist in given locations. Places all snapshots in <dest>,
and will remove old versions of the same snapshots.
"""

from sh import btrfs
import datetime
import re
import os

dirs = ('/home/zanny',)
dest = '/tmp/snapshots'


def take_snapshot(subvolume, destination):
    """ Take a btrfs snapshot of a given subvolume in a given destination. """
    name = subvolume.rpartition('/')[2]
    target = dest + '/' + name + datetime.date.today().isoformat()
    regex = re.compile(r'^' + re.escape(name) + r'_\d{4}-(0\d)|(1[0-2])-([0-2]\d)|(3[01])$')
    old = False
    for file in os.listdir(dest):
        match = regex.match(target)
        if match:
            old = match.group(0)
            break
    result = btrfs("subvolume snapshot", subvolume, target)
    print(result)
    if result.exit_code == 0:
        if old:
            result = btrfs("subvolume delete", old)
            print(result)
            if result.exit_code == 0:
                print('Old copy of ' + name + ' removed: ' + old)
            else:
                raise RuntimeError('Error in deleting discovered old snapshot: ' + old)
    else:
        raise RuntimeError('Non-0 return code when snapshotting: ' + name + ' to location: ' + target)


def makedest(destination):
    """ Creates the destination only if it does not already exist."""
    if not os.path.exists(dest):
        os.mkdir(dest)

if os.getuid == 0:
    if prompt('Warning! Executing as root - potentially dangerous, make sure your dirs and dest are right. '
              'Enter y to proceed. ') == 'y':
        for dir in dirs:
            take_snapshot(dir, dest)
    else:
        if not os.access(dest, os.W_OK):
            print('Unable to write to specified destination: ' + dest)
        else:
            for dir in dirs:
                if not os.acccess(dir, os.R_OK):
                    print('Lacking read access to specified subvolume: ' + dir)
                else:
                    take_snapshot(dir, dest)